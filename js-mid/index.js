Function.prototype.construct = function(args) {
  var oNew = Object.create(this.prototype)
  this.apply(oNew, args)
  return oNew
};

function myConstructor () {
  for(var i = 0; i < arguments.length; i++) {
    this['prop' + i] = arguments[i]
  }
}

var arr = ['abc', 'hello', 123, false]

var myInstance = myConstructor.construct(arr)
console.log(myInstance.prop0) // 'abc'
// myInstance 是不是myConstructor构造出来的
console.log(myInstance instanceof myConstructor) // true
console.log(myInstance.constructor) // myConstructor


var nums = [1, 9, 23, 20, 15, 8, 3, 6]

var maxer = Math.max.apply(null, nums)
var miner = Math.min.apply(null, nums)

console.log(maxer) // 23
console.log(miner) // 1


function minOfArray(arr) {
  var min = Infinity
  var QUANTUM = 32768

  for(var i = 0, len = arr.length; i < len; i += QUANTUM) {
    var submin = Math.min.apply(null, arr.slice(i, Math.min(i+QUANTUM, len)))
    min = Math.min(submin, min)
  }

  return min
}

var min = minOfArray([5, 488, 2, 3, 7])
console.log(min)

var obj = {
  foo: function() {
    console.log(1)
  }
}

var originFn = obj.foo

obj.foo = function() {
  console.log(arguments)
  originFn.apply(this, arguments)

  console.log(2)
}

obj.foo(123, 345)




