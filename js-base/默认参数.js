function foo() {
  return 'foo'
}

function withDefault(a, b = 2, c = b, d = foo(), e = this, f = arguments, g = this.value) {
  return [a, b, c, d, e, f, g]
}

var res = withDefault.call({value: "\(^o^)/~"})
console.log(res) // [ undefined, 2, 2, 'foo', { value: '(^o^)/~' }, {}, '(^o^)/~' ]

function withoutDefault(a, b, c, d, e, f, g) {
  switch(arguments.length) {
    case 0:
      a;
    case 1:
      b = 2;
    case 2:
      c = b;
    case 3:
      d = foo();
    case 4:
      e = this;
    case 5:
      f = arguments;
    case 6:
      g = this.value;
    default:
      console.log('default !')
  }
  return [a, b, c, d, e, f, g]
}

res = withoutDefault.call({value: "\(^o^)/~"})
console.log(res) // [ undefined, 2, 2, 'foo', { value: '(^o^)/~' }, {}, '(^o^)/~' ]
