function addSquare(x, y) {
  function square (x) {
    return x * x
  }
  return square(x) + square(y)
}

var a = addSquare(2, 3)
var b = addSquare(4, 5)