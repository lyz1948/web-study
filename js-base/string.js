
var str = new String('abcd')
console.log(str) // abcd
console.log(typeof str) // object

var res = str.charAt(2) // c
res = str.charCodeAt(2) // 99
res = str.codePointAt(1) // 98

var str = 'hello world!'
res = str.indexOf('w') // 6
res = str.lastIndexOf('l') // 9

res = str.startsWith('h') // true
res = str.startsWith('e') // false

res = str.endsWith('!') // true
res = str.endsWith('d') // false

res = str.includes('lo') // true
res = str.includes('le') // false

var str2 = ' I am come on!'
res = str.concat(str2) // hello world! I am come on!

res = String.fromCharCode(65, 66, 67) // ABC
res = String.fromCharCode(97, 98, 99) // abc

res = String.fromCodePoint(49) // 1
res = String.fromCodePoint(65, 90) // AZ

res = str.split(' ') // ['hello', 'world']
res = str.split('') // [ 'h', 'e', 'l', 'l', 'o', ' ', 'w', 'o', 'r', 'l', 'd', '!' ]

res = str.slice(0, 3) //hel
res = str.slice(0, -1) // hello world
res = str.slice(2) // llo world!
res = str.slice(-2) // d!

res = str.substring(1) // ello world!
res = str.substring(3, 8) // lo wo
res = str.substring(-2, 3) // hel
res = str.substring(0) // hello world!

res = str.substr(1) // ello world!
res = str.substr(3, 8) // lo world
res = str.substr(-2, 3) // d!

var re = /llo/
res = str.match(re) // [ 'llo', index: 2, input: 'hello world!' ]

res = str.replace(re, '--') // he-- world!

function replacer(match, p1, p2, p3, offset, string) {
  console.log(match)
  console.log(offset, string)
  return [p1, p2, p3].join('-')
}

res = 'abcd1234*#@!789'.replace(/([^\d]*)(\d*)([^\w]*)/, replacer)

var str = 'Appler are round, and apples are round'
var re = /apple/gi

res = str.replace(re, 'orange') // oranger are round, and oranges are round

var str = 'hello world'
res = str.replace(/(\w*)\s(\w*)/, '$2, $1')

var str = 'x-x_';
var retArr = [];
str.replace(/(x_*)|(-)/g, function(match, p1, p2) {
  if (p1) { retArr.push({ on: true, length: p1.length }); }
  if (p2) { retArr.push({ on: false, length: 1 }); }
});

console.log(retArr)

console.log(res)
