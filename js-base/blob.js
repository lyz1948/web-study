// // 使用 Blob 创建一个指向类型化数组的URL
// var typedArray = GetTheTypedArraySomehow();
// var blob = new Blob([typedArray], {type: "application/octet-binary"});// 传入一个合适的MIME类型
// var url = URL.createObjectURL(blob);
// console.log(url)

var data='<b style="font-size:32px;color:red;">次碳酸钴</b>';   
var blob = new Blob([data],{"type":"text/html"});   
console.log(blob);

// 从 Blob 中提取数据
var reader = new FileReader()

reader.addEventListener('loadend', function() {
  console.log(reader.result)
})

reader.readAsArrayBuffer(blob)