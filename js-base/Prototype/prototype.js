// 雇员原型
function Employee() {
  this.name = ''
  this.dept = 'general'
}

// 管理原型
function Manager() {
  Employee.call(this)
  this.reports = []
}

Manager.prototype = Object.create(Employee.prototype)

function WorkerBee() {
  Manager.call(this)
  this.projects = []
}

WorkerBee.prototype = Object.create(Manager.prototype)

function SalesPerson() {
  WorkerBee.call(this)
  this.dept = 'sales'
  this.quota = 100
}

SalesPerson.prototype = Object.create(WorkerBee.prototype)

function Engineer() {
  WorkerBee.call(this)
  this.dept = 'engineering'
  this.machine = ''
}

Engineer.prototype = Object.create(WorkerBee.prototype)

var jim = new Employee()
console.log(jim.name) // ''
console.log(jim.dept) // 'general'

var sally = new Manager()
sally.name  // ''
sally.dept  // 'general'
sally.reports // []

var mark = new WorkerBee()
mark.name // ''
mark.dept // 'general'
mark.projects // []

var fred = new SalesPerson()
fred.name // ''
fred.dept // 'sales'
fred.projects  // []
fred.quota // 100

var john = new Engineer()
john.name // ''
john.dept // 'engineering'
john.reports // []

// 更灵活的构造器
function Employee(name, dept) {
  this.name = name || ''
  this.dept = dept || 'general'
}



