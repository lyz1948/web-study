function fun() {
  try {
    console.log(0)
    throw "bogus"
  } catch(e) {
    console.log(1)
    return true
    console.log(2)
  } finally {
    console.log(3)
    return false
    console.log(4)
  }
  console.log(5)
} 
//fun()  // 0 1 3

function fun() {
  try {
    throw 'bogus'
  } catch(e) {
    console.log('caught inner bogus')
    throw e 
  } finally {
    return false
  }
}

try {
  fun() // 'caught inner bogus'
} catch(e) {
  console.log('caught outer bogus')
}

for(var i = 0; i < 5; i++) {
  if(i == 3) {
    continue
    console.log('continue', i)
  }
  console.log(i)
}

