// 在es6之前的代码
function f(a, b) {
  var args = Array.prototype.slice.call(arguments, f.length)
  // ...
}

// 相当于下面的代码
function f(a, b, ...args) {
  // todo
}

// 结构剩余参数
function f(...[a, b, c]) {
  return a + b + c
}

f(1) // NaN  b 和 c 是 undefined
f(1, 2, 3) // 6
f(1, 2, 3, 4) // 6 只接收3个参数

function f(...args) {
  console.log(args.length)
}

f() // 0
f(1, 2) // 2

// 剩余参数包含了从第二个参数到最后一个参数
function multiply(num, ...args) {
  return args.map(function(it) {
    return num * it
  })
}

// 剩余参数拥有数组的所有方法，arguments不行
function sortRestArgs(...args) {
  return args.sort()
}

var res = sortRestArgs(5, 2, 4, 6, 3)
console.log(res)


function sortArguments() {
  return arguments.sort()
}

var resArgs = sortArguments(5, 2, 4, 6, 3)
console.log(resArgs) // TypeError: arguments.sort is not a function

// arguments可以先转换成数组，之后就可以使用数组的方法了
function sortArguments() {
  var args = Array.prototype.slice.call(arguments)
  return args.sort()
}

var resArgs = sortArguments(5, 2, 4, 6, 3)
console.log(resArgs)
