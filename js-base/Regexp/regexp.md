正则的写法
```js
var re = new RegExp('a');
var re = /a/;
```

- 正则默认：匹配成功就结束，不会继续匹配
- 如果想全部查找，就要加g（全局匹配）
- 量词：匹配不确定的位置
- +： 至少出现一次

正则中的方法：test() match() replace() search()

- []方括号的用法
[abc]     括号内的任意字符匹配均可以
[a-z]     范围 a-z 的范围内
[h-m]     h - m 范围
[A-Z]     范围 A-Z 的范围内
[0-9]     范围 0-9 的范围内

$     结尾
^         行首
[^]       排除
[^a-z]    除了字母
[^0-9]    除了数字

. (点)      任意字符
\. 真正的点需要转义

- 转义字符
\d 数字    [0-9]
\D 非数字   [^0-9]

\w 字符 英文、数字、下划线 [a-z0-9_]
\W 非字符   [^a-z0-9_]

\s 空格 空白字符
\S 非空格 非空白字符

\b 独立部分（起始、结束、空格）
\B 独立部分

\1 重复的第一个子项
\2 重复的第二个子项

- 量词
{n}     正好出现n次
{n,m}   最少出现n次，最多m次
{n,}    最少出现n次，最多不限

+        {1,}     最少出现1次，最多不限
？       {0,1}    最少出现0次就是可以不出现，最多1次
*        {0,}     最少出现0次，最多不限 (谨慎使用)
