function map(f, a) {
  var result = [], i

  for(i = 0; i != a.length; i++) 
    result[i] = f(a[i])
  return result
}

var f = function(n) {
  return n * n * n 
}

var number = [1, 2, 3, 4, 5]

var cube = map(f, number)

console.log(cube) // [ 1, 8, 27, 64, 125 ]

var name = 'global'
function globalFun() {
  console.log(name)
}

function localFun() {
  var name = 'local'
  console.log(name)
}

globalFun() // 'global'
localFun() // 'local'