var arr = ['hello', 'my world']

// 非箭头函数写法
var res1 = arr.map(function(it) {
  return it.length
})
console.log(res1) // [5, 8]

// 箭头函数简洁明了
var res2 = arr.map(it => it.length)
console.log(res2) // [5, 8]
